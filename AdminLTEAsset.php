<?php
/*
 * 2014-10-24
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 */

namespace prawee\theme\adminlte;

use yii\web\AssetBundle;

class AdminLTEAsset extends AssetBundle
{
    public $sourcePath = '@vendor/prawee/yii2-theme-admin-lte/src';
    public $baseUrl    = '@web';
    public $css        = [
        'css/ionicons.min.css',
        'css/morris/morris.css',
        'css/jvectormap/jquery-jvectormap-1.2.2.css',
        'css/datepicker/datepicker3.css',
        'css/daterangepicker/daterangepicker-bs3.css',
        'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'css/AdminLTE.css',
    ];
    public $js         = [
        'js/jquery-1.11.0.js',
        'js/plugins/morris/raphael.min.js',
        'js/plugins/morris/morris.min.js',
        'js/plugins/morris/morris-data.js',
    ];
    public $depends    = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init()
    {
        parent::init();
    }
}